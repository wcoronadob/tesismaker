from django.urls import path
from .views import ProductsListViews

urlpatterns = [
    path('/', ProductsListViews.as_view(), name='Products-List-View'),
]